# Notes:
* After database refresh, need to run
```
php artisan passport:install
```

# How to run:
* Create .env file from .env.example
* Composer install
* Run
```
php artisan key:generate
```
* php artisan serve

# Run migration
1. php artisan migrate
2. php artisan migrate:fresh // drop all tables, run migrations

# Run seeder
1. php artisan db:seed

# Passport generate
1. php artisan passport:install

# Generate swagger
1. php artisan l5-swagger:generate

# Model:
* User (DONE)
* Keranjang belanjaan (DONE)
* Transaksi (DONE)
* Feedback (DONE)
* Payment Method (DONE)
* Status Transaksi (DONE)
* Notification (DONE)
* Address (DONE)
* Detail Transaksi (DONE)
* Admin (DONE)
* Admin Role (DONE)
* Unit (DONE)
* Resep Type (DONE)
* Resep Kategori (DONE)
* Resep (DONE)
* Cooking Planner (DONE)
* Merchant (DONE)
* Store Address (DONE)
* Global Setting (DONE)
* Resep Favorit (DONE)
* Rating (DONE)
* Promo (DONE)
* Product (DONE)
* Product Category (DONE)
* Kategori (DONE)

# Factory:
* User (DONE)
* Keranjang belanjaan ()
* Transaksi ()
* Feedback ()
* Payment Method ()
* Status Transaksi ()
* Notification ()
* Address ()
* Detail Transaksi ()
* Admin (DONE)
* Admin Role (DONE)
* Unit (DONE)
* Resep Type (DONE)
* Resep Kategori (DONE)
* Resep (DONE)
* Cooking Planner ()
* Merchant (DONE)
* Store Address ()
* Global Setting ()
* Resep Favorit ()
* Rating ()
* Promo ()
* Product (DONE)
* Product Category (DONE)
* Kategori ()