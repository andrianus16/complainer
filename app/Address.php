<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * @OA\Schema(
 * @OA\Xml(name="Address"),
 * @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 * @OA\Property(property="id_user", type="string", readOnly="true", description="User role"),
 * @OA\Property(property="label", type="string", description="address", example="Alamat 1"),
 * @OA\Property(property="alamat", type="string", description="Datetime marker of verification status", example="jl kemayoran"),
 * @OA\Property(property="latitude", type="float", maxLength=10, example=90.5),
 * @OA\Property(property="longitude", type="float", maxLength=10, example=10.6),
 * @OA\Property(property="is_primary", type="boolean", example=true)
 * )
 */

class Address extends Model
{
    protected $guarded = [
        'id'
    ];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
