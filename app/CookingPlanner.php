<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CookingPlanner extends Model
{
    protected $guarded = [
        'id'
    ];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function recipe()
    {
        return $this->hasOne('App\Resep', 'id', 'id_recipe');
    }
}
