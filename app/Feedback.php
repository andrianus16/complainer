<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'nama_pengirim', 'link_email', 'link_whatsapp', 'feedback'
    ];
}
