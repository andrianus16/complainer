<?php

namespace App\Helpers;

class Token
{
  public static function isFacebookTokenValid($access_token)
  {
    $app_token = static::getFacebookAppToken();
    $response = \Http::get(sprintf('https://graph.facebook.com/debug_token?input_token=%s&access_token=%s', $access_token, $app_token));
    $data = $response->json();
    return $response->status() == 200 && $data["data"]["is_valid"];
  }
  private static function getFacebookAppToken()
  {
    $response = \Http::get(sprintf('https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&grant_type=client_credentials', env("FB_ID"), env("FB_SECRET")));
    $data = $response->json();
    return $data["access_token"];
  }
  public static function isGoogleTokenValid($access_token)
  {
    $response = \Http::get(sprintf('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s', $access_token));
    $data = $response->json();
    if(isset($data["error"]))
    {
      return false;
    }
    return true;
  }
}
