<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Address;

class AddressController extends Controller
{
    /**
     * @OA\Get(
     * path="/addresses",
     * summary="Get address",
     * description="Get address",
     * operationId="getAddresses",
     * tags={"Address"},
     * security={
     *   {"bearer": {}},
     * },
     * @OA\Response(
     *    response=200,
     *    description="Success get address",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Success get address"),
     *       @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              ref="#/components/schemas/Address"
     *          )
     *       ),
     *       @OA\Property(property="success", type="boolean", example=true)
     *       )
     *    ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthorized",
     *      @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Unauthorized"),
     *          @OA\Property(property="success", type="boolean", example=false)
     *      )
     *   )
     * )
     */
    public function addresses()
    {
        $user_id = auth()->user()->id;
        $addresses = Address::where("id_user", $user_id)->get();
        return response()->json(\Response::success("Success get address", $addresses));
    }
    
    /**
     * @OA\Post(
     * path="/address",
     * summary="Create address",
     * description="Get address",
     * operationId="createAddress",
     * tags={"Address"},
     * security={
     *   {"bearer": {}},
     * },
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass param",
     *    @OA\JsonContent(
     *       required={"label","alamat", "latitude", "longitude", "is_primary"},
     *       @OA\Property(property="label", type="string", example="Alamat 1"),
     *       @OA\Property(property="alamat", type="string", example="Jl sunter"),
     *       @OA\Property(property="latitude", type="float", example=-22.223948),
     *       @OA\Property(property="longitude", type="float", example=22.223948),
     *       @OA\Property(property="is_primary", type="boolean", example=true),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Success"
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Unauthorized"),
     *       @OA\Property(property="success", type="boolean", example=false)
     *      )
     *   )
     * )
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'label' => 'required|string|max:50',
            'alamat' => 'required|string|max:255',
            'latitude' => 'required|numeric|between:-90,90',
            'longitude' => 'required|numeric|between:-180,180',
            'is_primary' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json(\Response::error_without_data(
                "Bad Request",
                ["error" => $validator->errors()]
            ), 400);
        }

        $user_id = auth()->user()->id;

        if($request->is_primary){
            Address::where("id_user", $user_id)->update(['is_primary' => false]);
        }

        $address = Address::create([
            'id_user' => $user_id,
            'label' => $request->label,
            'alamat' => $request->alamat,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'is_primary' => $request->is_primary,
        ]);
        $address->save();

        return response()->json(\Response::success("Address created", $address));
    }
    
    /**
     * @OA\Put(
     * path="/address/{address_id}/makePrimary",
     * summary="Make Primary address",
     * description="Make Primary address",
     * operationId="makePrimaryAddress",
     * tags={"Address"},
     * security={
     *   {"bearer": {}},
     * },
     * @OA\Parameter(name="address_id",in="path",required=true,example="1"),
     * @OA\Response(
     *    response=200,
     *    description="Success"
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Unauthorized"),
     *       @OA\Property(property="success", type="boolean", example=false)
     *      )
     *   )
     * )
     */
    public function makePrimary($id)
    {
        $address = Address::find($id);
        if($address){
            $user_id = auth()->user()->id;
            Address::where("id_user", $user_id)->update(['is_primary' => false]);

            $address->is_primary = true;
            $address->save();
            return response()->json(\Response::success("Address updated", $address));
        }
        return response()->json(\Response::error_without_data("Address not found"));
    }
    
    /**
     * @OA\Put(
     * path="/address/{address_id}",
     * summary="Update address",
     * description="Update address",
     * operationId="updateAddress",
     * tags={"Address"},
     * security={
     *   {"bearer": {}},
     * },
     * @OA\Parameter(name="address_id",in="path",required=true,example="1"),
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass param",
     *    @OA\JsonContent(
     *       required={"label","alamat", "latitude", "longitude", "is_primary"},
     *       @OA\Property(property="label", type="string", example="Alamat 1"),
     *       @OA\Property(property="alamat", type="string", example="Jl sunter"),
     *       @OA\Property(property="latitude", type="float", example=-22.223948),
     *       @OA\Property(property="longitude", type="float", example=22.223948),
     *       @OA\Property(property="is_primary", type="boolean", example=true),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Success"
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Unauthorized"),
     *       @OA\Property(property="success", type="boolean", example=false)
     *      )
     *   )
     * )
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'label' => 'required|string|max:50',
            'alamat' => 'required|string|max:255',
            'latitude' => 'required|numeric|between:-90,90',
            'longitude' => 'required|numeric|between:-180,180',
            'is_primary' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json(\Response::error_without_data(
                "Bad Request",
                ["error" => $validator->errors()]
            ), 400);
        }

        $user_id = auth()->user()->id;

        $address = Address::where([
            ["id", $id],
            ["id_user", $user_id]
        ])->first();

        if($address){
            if($request->is_primary){
                Address::where("id_user", $user_id)->update(['is_primary' => false]);
            }

            $address->id_user = $user_id;
            $address->label = $request->label;
            $address->alamat = $request->alamat;
            $address->latitude = $request->latitude;
            $address->longitude = $request->longitude;
            $address->is_primary = $request->is_primary;
            $address->save();
            return response()->json(\Response::success("Address updated", $address));
        }
        return response()->json(\Response::error_without_data("Address not found"));
    }

    /**
     * @OA\Delete(
     * path="/address/{address_id}",
     * summary="Delete address",
     * description="Delete address",
     * operationId="deleteAddress",
     * tags={"Address"},
     * security={
     *   {"bearer": {}},
     * },
     * @OA\Parameter(name="address_id",in="path",required=true,example="1"),
     * @OA\Response(
     *    response=200,
     *    description="Success"
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Unauthorized"),
     *       @OA\Property(property="success", type="boolean", example=false)
     *      )
     *   )
     * )
     */
    public function destroy($id)
    {
        $address = Address::find($id);
        if($address){
            $address->delete();
            return response()->json(\Response::success("Address deleted", $address));
        }
        return response()->json(\Response::error_without_data("Address not found"));
    }
}
