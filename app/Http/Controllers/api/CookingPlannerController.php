<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CookingPlanner;

class CookingPlannerController extends Controller
{
    /**
     * @OA\Get(
     * path="/cooking-planner",
     * summary="Get cooking planner",
     * description="Get cookingplanner",
     * operationId="getCookingPlanner",
     * tags={"Cooking Planner"},
     * security={
     *   {"bearer": {}},
     * },
     * @OA\Parameter(name="date",in="query",required=true,example="2020-07-22"),
     * @OA\Response(
     *    response=200,
     *    description="Success",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Success"),
     *       @OA\Property(
     *          property="data",
     *          type="array",
     *          @OA\Items(
     *              type="object",
     *              @OA\Property(property="id",type="int",example="1"),
     *              @OA\Property(property="id_recipe",type="int",example="1"),
     *              @OA\Property(property="time",type="string",example="Malam"),
     *              @OA\Property(property="date",type="string",example="2020-07-22"),
     *              @OA\Property(property="recipe_name",type="string",example="Nasi goreng"),
     *          )
     *       ),
     *       @OA\Property(property="success", type="boolean", example="true")
     *       )
     *    ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthorized",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Unauthorized"),
     *       @OA\Property(property="success", type="boolean", example=false)
     *      )
     *   )
     * )
     */
    public function index(Request $request)
    {
        $date = $request->date;
        // $cookingPlanner = CookingPlanner::with("recipe:id,title")->where([
        //     ["id_user", auth()->user()->id],
        //     ["date", $date]
        // ])->get();
        $cookingPlanner = CookingPlanner::where([
                ['cooking_planners.id_user', auth()->user()->id],
                ['cooking_planners.date', $date],
            ])
            ->leftJoin('reseps', 'cooking_planners.id_recipe', '=', 'reseps.id')
            ->select(
                'cooking_planners.*',
                'reseps.title as recipe_name'
            )->get();
        return response()->json(\Response::success("Success get cooking planner", $cookingPlanner));
    }
    /**
     * @OA\Get(
     *  path="/cooking-planner/list-date",
     *  tags={"Cooking Planner"},
     *  summary="List Cooking Planner Date",
     *  operationId="listDate",
     *  security={
     *      {"bearer": {}},
     *  },
     * @OA\Parameter(name="month",in="query",required=true,example="7"),
     * @OA\Parameter(name="year",in="query",required=true,example="2020"),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthorized",
     *      @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Unauthorized"),
     *          @OA\Property(property="success", type="boolean", example=false)
     *      )
     * )
     *)
     **/
    public function listDate(Request $request)
    {
        $month = $request->month;
        $year = $request->year;

        $cookingPlanner = CookingPlanner::where('cooking_planners.id_user', auth()->user()->id)
            ->whereMonth('cooking_planners.date', $month)
            ->whereYear('cooking_planners.date', $year)
            ->select('cooking_planners.date')->groupBy('date')->pluck("date");
        return response()->json(\Response::success("Success get cooking planner", $cookingPlanner));
    }
    
    /**
     * @OA\Post(
     *  path="/cooking-planner",
     *  tags={"Cooking Planner"},
     *  summary="Create Cooking Planner",
     *  operationId="createCookingPlanner",
     *  security={
     *      {"bearer": {}},
     *  },
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass param",
     *    @OA\JsonContent(
     *       required={"id_recipe","time", "date"},
     *       @OA\Property(property="id_recipe", type="int", example=1),
     *       @OA\Property(property="time", type="string", example="Malam"),
     *       @OA\Property(property="date", type="string", example="2020-07-22"),
     *    ),
     * ),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthorized",
     *      @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Unauthorized"),
     *          @OA\Property(property="success", type="boolean", example=false)
     *      )
     * )
     *)
     **/
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id_recipe' => 'required|exists:reseps,id',
            'date' => 'required|date',
            'time' => 'required|in:Pagi,Siang,Malam'
        ]);

        if ($validator->fails()) {
            return response()->json(\Response::error_without_data(
                "Bad Request",
                ["error" => $validator->errors()]
            ), 400);
        }

        $cookingPlanner = CookingPlanner::create([
            'id_user' => auth()->user()->id,
            'id_recipe' => $request->id_recipe,
            'date' => $request->date,
            'time' => $request->time
        ]);
        $cookingPlanner->save();

        return response()->json(\Response::success("Cooking planner created", $cookingPlanner));
    }
    
    /**
     * @OA\Delete(
     *  path="/cooking-planner/{cookingplanner_id}",
     *  tags={"Cooking Planner"},
     *  summary="Delete Cooking Planner",
     *  operationId="deleteCookingPlanner",
     *  security={
     *      {"bearer": {}},
     *  },
     * @OA\Parameter(name="cookingplanner_id",in="path",required=true,example=1),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthorized",
     *      @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Unauthorized"),
     *          @OA\Property(property="success", type="boolean", example=false)
     *      )
     * )
     *)
     **/
    public function destroy($id)
    {
        $cookingPlanner = CookingPlanner::find($id);
        if($cookingPlanner && $cookingPlanner->id_user==auth()->user()->id){
            $cookingPlanner->delete();
            return response()->json(\Response::success("Cooking planner deleted", $cookingPlanner));
        }
        return response()->json(\Response::error_without_data("Cooking planner not found"));
    }
}
