<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Feedback;
use App\Mail\MailFeedback;

class FeedbackController extends Controller
{
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'link_email' => 'required|string|email|max:255',
            'link_whatsapp' => 'required|string|max:255',
            'feedback' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'error' => $validator->errors()
            ), 400);
        }

        $feedback = Feedback::create([
            'link_whatsapp' => $request->link_whatsapp,
            'link_email' => $request->link_email,
            'nama_pengirim' => $request->name,
            'feedback' => $request->feedback,
        ]);

        $to = 'ivanzulf.0214@gmail.com';
        $response = Mail::to($to)->send(new MailFeedback(
            $feedback->nama_pengirim, $feedback->link_email, $feedback->link_whatsapp, $feedback->feedback
        ));

        return response()->json(\Response::success("Feedback sent", $feedback), 200);
    }
}
