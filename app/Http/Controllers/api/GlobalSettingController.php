<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\GlobalSetting;

class GlobalSettingController extends Controller
{
    public function index(Request $request) {
        $key = trim($request->query("key"));

        $about = GlobalSetting::where("key", $key)->select('value')->first();

        if ($about != null) {
            $response[$key] = $about->value;
            return response()->json(\Response::success("Success get about", $response), 200);
        }

        return response()->json(\Response::error_without_data("Global key not found"), 404);
    }
}
