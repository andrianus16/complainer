<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Merchant;
use App\ProductCategory;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->query("per_page", \Resx::DEFAULT_PERPAGE);
        $page = $request->query("page");
        $limit = $request->query("limit");

        $merchant = Merchant::where("status","active");

        if($page)
        {
            // With paging
            $merchant = $merchant->paginate($per_page);
            return response()->json(\Response::success_without_data("Success get merchant", $merchant->toArray()));
        }
        else
        {
            // without paging
            if($limit)
            {
                $merchant = $merchant->limit($limit);
            }
            $merchant = $merchant->get();
            return response()->json(\Response::success("Success get merchant", $merchant));
        }
    }

    public function categories($id_merchant, Request $request)
    {
        $per_page = $request->query("per_page", \Resx::DEFAULT_PERPAGE);
        $page = $request->query("page");
        $limit = $request->query("limit");

        $product_category = ProductCategory::where("id_merchant", $id_merchant);
        // $product = Product::leftJoin('ingredients', 'products.id_ingredient', '=', 'ingredients.id')
        //     ->leftJoin('ingredient_categories as ing1', 'ingredients.id_category', '=', 'ing1.id')
        //     ->leftJoin('ingredient_categories as ing2', 'ing1.parent', '=', 'ing2.id')
        //     ->where("id_merchant", $id_merchant)
        //     ->where(function($query)
        //     {
        //         $query->where("ing1.level", 0)
        //         ->orWhere("ing2.level", 0);
        //     })
        //     ->select(
        //         \DB::raw("COALESCE(ing1.id, ing2.id) as category_id"),
        //         \DB::raw("COALESCE(ing1.name, ing2.name) as category_name")
        //     )
        //     ->distinct();
        if($page)
        {
            // With paging
            $product_category = $product_category->paginate($per_page);
            return response()->json(\Response::success_without_data("Success get categories", $product_category->toArray()));
            // $product = $product->paginate($per_page);
            // return response()->json(\Response::success_without_data("Success get categories", $product->toArray()));
        }
        else
        {
            // without paging
            if($limit)
            {
                $product_category = $product_category->limit($limit);
                // $product = $product->limit($limit);
            }
            $product_category = $product_category->get();
            return response()->json(\Response::success("Success get categories", $product_category));
            // $product = $product->get();
            // return response()->json(\Response::success("Success get categories", $product));
        }
    }

    public function detail($id_merchant)
    {
        $merchant = Merchant::find($id_merchant);
        if($merchant)
        {
            return response()->json(\Response::success("Success get merchant", $merchant));
        }
        return response()->json(\Response::error_without_data("Merchant not found"));
    }
    
}
