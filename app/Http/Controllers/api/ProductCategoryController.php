<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProductSubCategory;

class ProductCategoryController extends Controller
{
    public function subcategories($id_category, Request $request)
    {
        $per_page = $request->query("per_page", \Resx::DEFAULT_PERPAGE);
        $page = $request->query("page");
        $limit = $request->query("limit");
        
        $product_subcategory = ProductSubCategory::where("id_product_category", $id_category);
        if($page)
        {
            // With paging
            $product_subcategory = $product_subcategory->paginate($per_page);
            return response()->json(\Response::success_without_data("Success get sub categories", $product_subcategory->toArray()));
        }
        else
        {
            // without paging
            if($limit)
            {
                $product_subcategory = $product_subcategory->limit($limit);
            }
            $product_subcategory = $product_subcategory->get();
            return response()->json(\Response::success("Success get sub categories", $product_subcategory));
        }
    }
}
