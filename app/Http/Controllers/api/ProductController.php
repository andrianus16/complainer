<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;

class ProductController extends Controller
{
    /**
     * @OA\Get(
     *  path="/product/search",
     *  tags={"Product"},
     *  summary="Search Product",
     *  operationId="searchProduct",
     *  security={
     *      {"bearer": {}},
     *  },
     * @OA\Parameter(name="q",in="query",example="Ayam"),
     * @OA\Parameter(name="id_merchant",in="query",example=2),
     * @OA\Parameter(name="id_category",in="query",example=2),
     * @OA\Parameter(name="id_sub_category",in="query",example=1),
     * @OA\Parameter(name="min_price",in="query",example=1000),
     * @OA\Parameter(name="max_price",in="query",example=100000),
     * @OA\Parameter(name="sort_type",in="query",example="best_seller",
     *      @OA\Schema(type="string",enum={"best_seller", "deals", "highest_price", "lowest_price", "a_z", "z_a"})
     * ),
     * @OA\Parameter(name="per_page",in="query",example=2),
     * @OA\Parameter(name="page",in="query",example=1),
     * @OA\Parameter(name="limit",in="query",example=10),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthorized",
     *      @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Unauthorized"),
     *          @OA\Property(property="success", type="boolean", example=false)
     *      )
     * )
     *)
     **/
    public function search(Request $request)
    {
        $per_page = $request->query("per_page", \Resx::DEFAULT_PERPAGE);
        $page = $request->query("page");
        $limit = $request->query("limit");

        $product = new Product();

        // FILTER
        $q = trim($request->query("q"));
        $id_merchant = $request->query("id_merchant");
        $id_category = $request->query("id_category");
        $id_sub_category = $request->query("id_sub_category");
        $min_price = $request->query("min_price");
        $max_price = $request->query("max_price");

        if($q)
        {
            $searchTexts = preg_split('/\s+/', $q, -1, PREG_SPLIT_NO_EMPTY);
            $product = $product->where(function($q2) use ($searchTexts) {
                foreach($searchTexts as $searchText){
                    $q2->orWhere('products.name', 'like', "%{$searchText}%");
                }
            });
        }
        if($id_merchant)
        {
            $product = $product->where("products.id_merchant", $id_merchant);
        }
        if($id_category)
        {
            $product = $product->where("id_category", $id_category);
            // $product = $product->whereExists(function ($query) use ($id_category) {
            //     $query->select(\DB::raw(1))
            //         ->from('ingredients')
            //         ->leftJoin('ingredient_categories as ing1', 'ingredients.id_category', '=', 'ing1.id')
            //         ->leftJoin('ingredient_categories as ing2', 'ing1.parent', '=', 'ing2.id')
            //         ->whereRaw('ingredients.id = products.id_ingredient')
            //         ->where(function($query) use ($id_category)
            //         {
            //             $query->where("ing1.id", $id_category)
            //             ->orWhere("ing2.id", $id_category);
            //         });
            // });
        }
        if($id_sub_category)
        {
            $product = $product->where("id_product_sub_category", $id_sub_category);
            // $product = $product->whereExists(function ($query) use ($id_sub_category) {
            //     $query->select(\DB::raw(1))
            //             ->from('ingredients')
            //             ->leftJoin('ingredient_categories as ing1', 'ingredients.id_category', '=', 'ing1.id')
            //             ->leftJoin('ingredient_categories as ing2', 'ing1.parent', '=', 'ing2.id')
            //             ->whereRaw('ingredients.id = products.id_ingredient')
            //             ->where(function($query) use ($id_sub_category)
            //             {
            //                 $query->where("ing1.id", $id_sub_category)
            //                 ->orWhere("ing2.id", $id_sub_category);
            //             });
            // });
        }
        if($min_price)
        {
            $product = $product->where("price", ">=", $min_price);
        }
        if($max_price)
        {
            $product = $product->where("price", "<=", $max_price);
        }

        // SORT
        $sort_type = $request->query("sort_type", "best_seller");
        switch($sort_type){
            case "deals":
                $product = $product->orderBy("discount", "desc");
                break;
            case "highest_price":
                $product = $product->orderBy("price", "desc");
                break;
            case "lowest_price":
                $product = $product->orderBy("price", "asc");
                break;
            case "a_z":
                $product = $product->orderBy("products.name", "asc");
                break;
            case "z_a":
                $product = $product->orderBy("products.name", "desc");
                break;
            default:
                // TODO terlaris
                break;
        }


        $product = $product->leftJoin('units', 'products.id_unit', '=', 'units.id')
            ->select(
                "products.*",
                "units.name as unit_name",
                \DB::raw("IF(products.discount > 0, ROUND((products.price * (products.discount / 100))), products.price ) as discount_price")
            );

        if($page)
        {
            // With paging
            $product = $product->paginate($per_page);
            return response()->json(\Response::success_without_data("Success get product", $product->toArray()));
        }
        else
        {
            // without paging
            if($limit)
            {
                $product = $product->limit($limit);
            }
            $product = $product->get();
            return response()->json(\Response::success("Success get product", $product));
        }
    }
    
    /**
     * @OA\Get(
     *  path="/product/{id_product}",
     *  tags={"Product"},
     *  summary="Detail Product",
     *  operationId="detailProduct",
     *  security={
     *      {"bearer": {}},
     *  },
     * @OA\Parameter(name="id_product",in="path",required=true,example=1),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *          @OA\Property(property="data", type="object", example={}),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthorized",
     *      @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Unauthorized"),
     *          @OA\Property(property="success", type="boolean", example=false)
     *      )
     * )
     *)
     **/
    public function detail($id_product)
    {
        $product = Product::where("products.id", $id_product);
        if($product)
        {
            $product = $product->leftJoin('units', 'products.id_unit', '=', 'units.id')
                ->select(
                    "products.*",
                    "units.name as unit_name",
                    \DB::raw("IF(products.discount > 0, ROUND((products.price * (products.discount / 100))), products.price ) as discount_price")
                )->first();
            return response()->json(\Response::success("Success get product", $product));
        }
        return response()->json(\Response::error_without_data("Product not found"));
    }
    
    
    /**
     * @OA\Get(
     *  path="/product/{id_product}/similar",
     *  tags={"Product"},
     *  summary="Similar Product",
     *  operationId="similarProduct",
     *  security={
     *      {"bearer": {}},
     *  },
     * @OA\Parameter(name="id_product",in="path",required=true,example=1),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *          @OA\Property(property="data", type="object", example={}),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthorized",
     *      @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Unauthorized"),
     *          @OA\Property(property="success", type="boolean", example=false)
     *      )
     * )
     *)
     **/
    public function similar($id_product)
    {
        $product = Product::find($id_product);
        $similarProduct = [];
        if($product)
        {
            $similarProduct = Product::where([
                ["products.id_product_category", $product->id_product_category],
                ["products.id", "!=", $id_product]
            ]);
            $similarProduct = $similarProduct->leftJoin('units', 'products.id_unit', '=', 'units.id')
                ->select(
                    "products.id",
                    "products.name",
                    "products.price",
                    "products.discount",
                    "products.quantity",
                    "products.images",
                    "units.name as unit_name",
                    \DB::raw("IF(products.discount > 0, ROUND((products.price * (products.discount / 100))), products.price ) as discount_price")
                )->get();
        }
        return response()->json(\Response::success("Success get product", $similarProduct));
    }
}
