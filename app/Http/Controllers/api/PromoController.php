<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Promo;

class PromoController extends Controller
{
    public function index(Request $request)
    {
        $per_page = $request->query("per_page", \Resx::DEFAULT_PERPAGE);
        $page = $request->query("page");
        $limit = $request->query("limit");

        $id_merchant = $request->query("id_merchant");

        $promo = new Promo();

        if($id_merchant){
            $promo = $promo->where("id_merchant", $id_merchant);
        }

        if($page)
        {
            // With paging
            $promo = $promo->paginate($per_page);
            return response()->json(\Response::success_without_data("Success get promo", $promo->toArray()));
        }
        else
        {
            // without paging
            if($limit)
            {
                $promo = $promo->limit($limit);
            }
            $promo = $promo->get();
            return response()->json(\Response::success("Success get promo", $promo));
        }
    }

    public function detail($id_promo)
    {
        $promo = Promo::find($id_promo);
        if($promo)
        {
            return response()->json(\Response::success("Success get promo", $promo));
        }
        return response()->json(\Response::error_without_data("Promo not found"));
    }
}
