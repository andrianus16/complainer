<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Rating;

class RatingController extends Controller
{
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id_recipe' => 'required|exists:reseps,id',
            'value' => 'required|integer|between:1,5',
        ]);

        if ($validator->fails()) {
            return response()->json(\Response::error_without_data(
                "Bad Request",
                ["error" => $validator->errors()]
            ), 400);
        }

        $rating = Rating::where([
            ["id_recipe", $request->id_recipe],
            ["id_user", auth()->user()->id],
        ])->first();

        if ($rating != null) {
            $rating->value = $request->value;
            $rating->save();

            return response()->json(\Response::success_without_data("rating updated"));
        }

        $myRating = Rating::create([
            'id_user' => auth()->user()->id,
            'id_recipe' => $request->id_recipe,
            'value' => $request->value,
        ]);
        $myRating->save();

        return response()->json(\Response::success_without_data("rating saved"));
    }
}
