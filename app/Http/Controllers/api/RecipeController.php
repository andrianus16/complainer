<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Resep;
use App\ResepKategori;
use App\ResepTipe;
use App\ResepFavorit;
use App\Rating;

class RecipeController extends Controller
{
    public function recipes()
    {	
		$recipes = Resep::select(
            'reseps.id',
            'reseps.title',
			'reseps.images',
			'reseps.video',
            'reseps.production_time',
			'reseps.author',
		)
		->simplePaginate(12);
		
		$items = array();

		foreach ($recipes->items() as $recipe) {
			$rating = Rating::where([
				["id_recipe", $recipe->id],
			])
			->select(
				DB::raw('SUM(value) AS rating_sum'),
				DB::raw('COUNT(value) AS rating_count'),
			)->first();

			if ($rating->rating_count > 0) {
				$total_rating = (float) $rating->rating_sum / $rating->rating_count;
			} else {
				$total_rating = 0.0;
			}

			$favorited = ResepFavorit::where([
				["id_recipe", $recipe->id],
				["id_user", auth()->user()->id]
			])
			->select('id_recipe')
			->first();

			$items[] = array(
				'favorited' => ($favorited != null) ? true : false,
				'id' => $recipe->id,
				'title' => $recipe->title,
				'images' => json_decode($recipe->images),
				'video' => $recipe->video,
				'production_time' => $recipe->production_time,
				'author' => $recipe->author,
				'rating' => array(
					'total' => floatval($total_rating),
					'count' => $rating->rating_count
				),
			);
		}

		$response = array(
			'current_page' => $recipes->currentPage(),
			"next_page_url" => $recipes->nextPageUrl(),
			"per_page" => $recipes->perPage(),
			"recipes" => $items
		);

    	return response()->json(\Response::success("Success get recipes", $response), 200);
    }

    public function recipeDetail($recipeID)
    {	
		$recipe = Resep::all()->where('id', $recipeID)->first();

		if ($recipe != null) {
			// all rating
			$rating = Rating::where([
				["id_recipe", $recipe->id]
			])
			->select(
				DB::raw('SUM(value) AS rating_sum'),
				DB::raw('COUNT(value) AS rating_count'),
			)->first();

			if ($rating->rating_count > 0) {
				$total_rating = (float) $rating->rating_sum / $rating->rating_count;
			} else {
				$total_rating = 0.0;
			}

			// my rating
			$myRating = Rating::where([
				["id_recipe", $recipe->id],
				["id_user", auth()->user()->id],
			])
			->select('value')->first();

			// recipe category
			$recipeCategoryIDs = json_decode($recipe->id_recipe_category);
			$categories = ResepKategori::whereIn('id', $recipeCategoryIDs)->select('id','name')->get();
			
			// recipe type
			$recipeType = ResepTipe::where('id', $recipe->id_recipe_type)->select('name')->first();
			
			// check if favorited
			$favorited = ResepFavorit::where([
				["id_recipe", $recipe->id],
				["id_user", auth()->user()->id]
			])
			->select('id_recipe')
			->first();

			$response = array(
				'favorited' => ($favorited != null) ? true : false,
				'id' => $recipe->id,
				'title' => $recipe->title,
				'images' => json_decode($recipe->images),
				'video' => $recipe->video,
				'author' => $recipe->author,
				'ingredients' => json_decode($recipe->ingredients),
				// 'ingredients' => $recipeIngredients,
				'steps' => json_decode($recipe->steps),
				'production_time' => $recipe->production_time,
				'portion_min' => $recipe->portion_min,
				'portion_max' => $recipe->portion_max,
				'difficulty' => $recipe->difficulty,
				'categories' => $categories,
				'recipe_type' => $recipeType->name,
				'rating' => array(
					'total' => floatval($total_rating),
					'count' => $rating->rating_count
				),
				'my_rating' => ($myRating != null) ? $myRating->value : 0,
			);
	
			return response()->json(\Response::success("Success get recipes", $response), 200);
		}

		return response()->json(\Response::error_without_data("Recipe not found"), 404);
	}
	
	public function search(Request $request) {
		$resep = new Resep();

		// FILTER
		$q = trim($request->query("q"));
		$id_category = (int) $request->query("id_category");
		$duration = $request->query("duration");
		$portion_min = $request->query("portion_min");
		$portion_max = $request->query("portion_max");
		$difficulty = $request->query("difficulty");
		$type = $request->query("type");
		// $ingredientID = $request->query("ingredientID");

		if($q)
        {
            $searchTexts = preg_split('/\s+/', $q, -1, PREG_SPLIT_NO_EMPTY);
            $resep = $resep->where(function($q2) use ($searchTexts) {
                foreach($searchTexts as $searchText){
                    $q2->orWhere('reseps.title', 'like', "%{$searchText}%");
                }
            });
		}

		if ($id_category) {
			$resep = $resep->whereJsonContains('reseps.id_recipe_category',[$id_category]);
		}
		if ($duration) {
			if ($duration == "1") { // < 30 mins
				$resep = $resep->where('reseps.production_time', '<', 30);
			} else if ($duration == "2") { // 30 - 60 mins
				$resep = $resep->whereBetween('reseps.production_time', [30, 60]);
			} else if ($duration == "3") { // > 60 mins
				$resep = $resep->where('reseps.production_time', '>', 60);
			}
		}
		if ($portion_min && $portion_max) {
			if ($portion_max < 10) {
				$resep = $resep->where('reseps.portion_min', '>=', $portion_min);
				$resep = $resep->Where('reseps.portion_max', '<=', $portion_max);
			} else {
				$resep = $resep->where('reseps.portion_min', '>=', $portion_min);
			}
		}
		if ($difficulty) {
			$resep = $resep->where('reseps.difficulty', '=', $difficulty);
		}
		if ($type) {
			$resep = $resep->where('reseps.id_recipe_type', '=', $type);
		}
		// if ($ingredientID) {
		// 	$resep = $resep->where('resep_details.id_ingredient', '=', $ingredientID);
		// }


		// SORT
		$sort_type = $request->query("sort_type", "newest");
        switch($sort_type){	
            case "most_popular":
                $resep = $resep->orderBy("fav_count", "desc");
                break;
            case "highest_rating":
				$resep = $resep->orderBy("rating", "desc");
                break;
            default:
				$resep = $resep->orderBy("id", "desc");
                break;
        }
		
		$resep = $resep->select(
			DB::raw('SUM(ratings.value) / COUNT(ratings.value) AS rating'),
			DB::raw('COUNT(resep_favorits.id) AS fav_count'),
            'reseps.id',
            'reseps.title',
			'reseps.images',
			'reseps.video',
			'reseps.production_time',
			'reseps.difficulty',
			'reseps.author',
		)
		->leftJoin('resep_favorits', 'resep_favorits.id_recipe', '=', 'reseps.id')
		->leftJoin('ratings', 'ratings.id_recipe', '=', 'reseps.id')
		->groupBy('reseps.id')
		->simplePaginate(12);

		$items = array();

		foreach ($resep->items() as $recipe) {
			$rating = Rating::where([
				["id_recipe", $recipe->id],
			])
			->select(
				DB::raw('SUM(value) AS rating_sum'),
				DB::raw('COUNT(value) AS rating_count'),
			)->first();

			if ($rating->rating_count > 0) {
				$total_rating = (float) $rating->rating_sum / $rating->rating_count;
			} else {
				$total_rating = 0.0;
			}

			$favorited = ResepFavorit::where([
				["id_recipe", $recipe->id],
				["id_user", auth()->user()->id]
			])
			->select('id_recipe')
			->first();

			$items[] = array(
				'favorited' => ($favorited != null) ? true : false,
				'id' => $recipe->id,
				'title' => $recipe->title,
				'images' => json_decode($recipe->images),
				'video' => $recipe->video,
				'production_time' => $recipe->production_time,
				'author' => $recipe->author,
				'rating' => array(
					'total' => floatval($total_rating),
					'count' => $rating->rating_count
				),
			);
		}

		$response = array(
			'current_page' => $resep->currentPage(),
			"next_page_url" => $resep->nextPageUrl(),
			"per_page" => $resep->perPage(),
			"recipes" => $items
		);

    	return response()->json(\Response::success("Success get recipes", $response), 200);
	}
}
