<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Resep;
use App\ResepFavorit;
use App\Rating;
use Auth;

class ResepFavoritController extends Controller
{
    function index() {
        $favoriteRecipes = ResepFavorit::where([
            ['resep_favorits.id_user', auth()->user()->id],
        ])
        ->leftJoin('reseps', 'resep_favorits.id_recipe', '=', 'reseps.id')
        ->select(
            'resep_favorits.id AS favorite_id',
            'reseps.id',
            'reseps.title',
            'reseps.images',
            'reseps.video',
            'reseps.production_time',
            'reseps.author'
        )->simplePaginate(12);

        $items = array();

		foreach ($favoriteRecipes->items() as $recipe) {
			$rating = Rating::where([
				["id_recipe", $recipe->id]
			])
			->select(
				DB::raw('SUM(value) AS rating_sum'),
				DB::raw('COUNT(value) AS rating_count'),
			)->first();

			if ($rating->rating_count > 0) {
				$total_rating = (float) $rating->rating_sum / $rating->rating_count;
			} else {
				$total_rating = 0.0;
            }
            
			$items[] = array(
				'favorite_id' => $recipe->favorite_id,
				'id' => $recipe->id,
				'title' => $recipe->title,
				'images' => json_decode($recipe->images),
                'video' => $recipe->video,
                'production_time' => $recipe->production_time,
				'author' => $recipe->author,
				'rating' => array(
					'total' => floatval($total_rating),
					'count' => $rating->rating_count
				),
			);
		}

		$response = array(
			'current_page' => $favoriteRecipes->currentPage(),
			"next_page_url" => $favoriteRecipes->nextPageUrl(),
			"per_page" => $favoriteRecipes->perPage(),
			"recipes" => $items
		);

    	return response()->json(\Response::success("Success get favorite recipes", $response), 200);
    }

    public function toggle(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id_recipe' => 'required|exists:reseps,id',
        ]);

        if ($validator->fails()) {
            return response()->json(\Response::error_without_data(
                "Bad Request",
                ["error" => $validator->errors()]
            ), 400);
        }

        $favRecipe = ResepFavorit::where([
            ["id_recipe", $request->id_recipe],
            ["id_user", auth()->user()->id],
        ])->first();

        if ($favRecipe != null) {
            $favRecipe->delete();
            return response()->json(\Response::success_without_data("removed from favorite"));
        }

        $favoriteRecipe = ResepFavorit::create([
            'id_user' => auth()->user()->id,
            'id_recipe' => $request->id_recipe,
        ]);
        $favoriteRecipe->save();

        return response()->json(\Response::success("favorited", $favoriteRecipe));
    }
}
