<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Resep;
use App\ResepKategori;
use App\Rating;
use App\ResepFavorit;

class ResepKategoriController extends Controller
{
    public function index() {
        $categories = ResepKategori::all();
		foreach ($categories as $c) {
			$countResep = DB::table('reseps')->whereJsonContains('id_recipe_category',[$c->id])->count();

			$response[] = array(
				'id' => $c->id,
				'name' => $c->name,
				'image' => $c->image,
				'count' => $countResep,
			);
		}
    			
		return response()->json(\Response::success("Success get categories", $response), 200);
    }

    public function similarRecipeCategory($recipeID) {
        $recipe = Resep::find($recipeID);
        $similarCategory = [];
        if ($recipe) {
            $similarCategory = ResepKategori::whereIn('id', json_decode($recipe->id_recipe_category))
                                            ->select('id', 'name')
                                            ->get();

            return response()->json(\Response::success("Success get categories", $similarCategory), 200);
        }

        return response()->json(\Response::error_without_data("Recipe not found"), 404);

        
    }

    public function recipeByCategory($categoryID, Request $request)
    {	
        $categoryID = (int)$categoryID;

        $category = ResepKategori::all('id', 'name')->where('id', $categoryID)->first();

        if ($category != null) {
            $recipes = DB::table('reseps')
                     ->whereJsonContains('id_recipe_category',[$categoryID])
                     ->select('id','title','images','video','production_time','author')
                     ->simplePaginate(12);

            $response = array(
                'category' => array(
                    'id' => $category->id,
                    'name' => $category->name
                ),
                'recipes' => $recipes
            );

            $items = array();

            foreach ($recipes->items() as $recipe) {
                $rating = Rating::where([
                    ["id_recipe", $recipe->id],
                ])
                ->select(
                    DB::raw('SUM(value) AS rating_sum'),
                    DB::raw('COUNT(value) AS rating_count'),
                )->first();

                if ($rating->rating_count > 0) {
                    $total_rating = (float) $rating->rating_sum / $rating->rating_count;
                } else {
                    $total_rating = 0.0;
                }

                $favorited = ResepFavorit::where([
                    ["id_recipe", $recipe->id],
                    ["id_user", auth()->user()->id]
                ])
                ->select('id_recipe')
                ->first();

                $items[] = array(
                    'favorited' => ($favorited != null) ? true : false,
                    'id' => $recipe->id,
                    'title' => $recipe->title,
                    'images' => json_decode($recipe->images),
                    'video' => $recipe->video,
                    'production_time' => $recipe->production_time,
                    'author' => $recipe->author,
                    'rating' => array(
                        'total' => floatval($total_rating),
                        'count' => $rating->rating_count
                    ),
                );
            }

            $response = array(
                'current_page' => $recipes->currentPage(),
                "next_page_url" => $recipes->nextPageUrl(),
                "per_page" => $recipes->perPage(),
                "recipes" => $items
            );

            return response()->json(\Response::success("Success get recipe by category", $response), 200);
        }
        
        return response()->json(\Response::error_without_data("Category not found"), 404);
    }
}
