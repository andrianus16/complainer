<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\AccountActivation;
use Auth;

class UserController extends Controller
{
    public function facebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function google()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * @OA\Post(
     ** path="/auth/google/login",
     *  tags={"Auth"},
     *  summary="Google Login",
     *  operationId="googleLogin",
     *
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password","access_token"},
     *       @OA\Property(property="email", type="string", example=""),
     *       @OA\Property(property="password", type="string", example=""),
     *       @OA\Property(property="image_url", type="string", example=""),
     *       @OA\Property(property="access_token", type="string", example=""),
     *    ),
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *          @OA\Property(property="token", type="string", example="token"),
     *      ),
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   )
     *)
     **/
    public function google_login(Request $request){
        /*
        request: email, name, image_url, access_token
        */
        if(!\Token::isGoogleTokenValid($request->access_token)){
            return response()->json(\Response::error_without_data("Access token invalid"));
        }
        
        $user_db = User::where([
            ["email", $request->email],
            ["status", "active"]
        ])->first();
        if($user_db==null){
            $new_user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => '',
                'login_method' => 'google',
                'phone' => '',
                'dob' => '1990-01-01',
                'gender' => '',
                'image' => $request->image_url,
                'push_notif_key' => '',
                'status' => 'active',
                'activation_code' => '',
                'reset_code' => ''
            ]);
            $new_user->save();
            
            $token = $new_user->createToken('Bule')->accessToken;
            return response()->json(\Response::success_without_data("Success login with google", [
                "token" => $token
            ]));
        }
        else if ($user_db->login_method == "facebook"){
            return response()->json(\Response::error_without_data("Email not found"));
        }

        $token = $user_db->createToken('Bule')->accessToken;
        return response()->json(\Response::success_without_data("Success login with google", [
            "token" => $token
        ]));
    }

    /**
     * @OA\Post(
     ** path="/auth/facebook/login",
     *  tags={"Auth"},
     *  summary="Facebook Login",
     *  operationId="facebookLogin",
     *
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password","access_token"},
     *       @OA\Property(property="email", type="string", example=""),
     *       @OA\Property(property="password", type="string", example=""),
     *       @OA\Property(property="image_url", type="string", example=""),
     *       @OA\Property(property="access_token", type="string", example=""),
     *    ),
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *          @OA\Property(property="token", type="string", example="token"),
     *      ),
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   )
     *)
     **/
    public function facebook_login(Request $request){
        /*
        request: email, name, image_url, access_token
        */
        if(!\Token::isFacebookTokenValid($request->access_token)){
            return response()->json(\Response::error_without_data("Access token invalid"));
        }
        
        $user_db = User::where([
            ["email", $request->email],
            ["status", "active"]
        ])->first();
        if($user_db==null){
            $new_user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => '',
                'login_method' => 'facebook',
                'phone' => '',
                'dob' => '1990-01-01',
                'gender' => '',
                'image' => $request->image_url,
                'push_notif_key' => '',
                'status' => 'active',
                'activation_code' => '',
                'reset_code' => ''
            ]);
            $new_user->save();
            
            $token = $new_user->createToken('Bule')->accessToken;
            return response()->json(\Response::success_without_data("Success login with facebook", [
                "token" => $token
            ]));
        }
        else if ($user_db->login_method == "google"){
            return response()->json(\Response::error_without_data("Email not found"));
        }

        $token = $user_db->createToken('Bule')->accessToken;
        return response()->json(\Response::success_without_data("Success login with facebook", [
            "token" => $token
        ]));
    }
    public function facebook_callback(Request $request){
        $user = Socialite::driver('facebook')->stateless()->user();
        return $user->token;
    }
    public function google_callback(Request $request){
        $user = Socialite::driver('google')->stateless()->user();
        return $user->token;
    }


    /**
     * @OA\Post(
     ** path="/auth/login",
     *  tags={"Auth"},
     *  summary="Login",
     *  operationId="login",
     *
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password"},
     *       @OA\Property(property="email", type="string", example="ivan@gmail.com"),
     *       @OA\Property(property="password", type="string", example="123456"),
     *    ),
     * ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *          @OA\Property(property="token", type="string", example="token"),
     *      ),
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   )
     *)
     **/
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            $user = User::where([
                ["email", $credentials["email"]],
                ["status", "active"],
                ["login_method", "email"]
            ])->first();
            if ($user != null && Hash::check($credentials["password"], $user->password)) {
                if ($token = $user->createToken('Bule')->accessToken) {
                    return response()->json(\Response::success_without_data("Success login", [
                        "token" => $token
                    ]), 200);
                }
            }
        } catch (JWTException $e) {
            return response()->json(\Response::error_without_data('Could not create token'), 500);
        }
        return response()->json(\Response::error_without_data('Invalid Credentials'), 400);
    }

    /**
     * @OA\Post(
     ** path="/auth/register",
     *  tags={"Auth"},
     *  summary="Register",
     *  operationId="register",
     *
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"name","email","password","password_confirmation"},
     *       @OA\Property(property="name", type="string", example=""),
     *       @OA\Property(property="email", type="string", example=""),
     *       @OA\Property(property="password", type="string", example=""),
     *       @OA\Property(property="password_confirmation", type="string", example=""),
     *    ),
     * ),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *      )
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     * )
     *)
     **/
    public function register(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'error' => $validator->errors()
            ), 400);
        }

        $user = $user->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'login_method' => 'email',
            'phone' => '',
            'dob' => '1990-01-01',
            'gender' => '',
            'image' => '',
            'push_notif_key' => '',
            'status' => 'inactive',
            'activation_code' => mt_rand(1000, 9999),
            'reset_code' => ''
        ]);

        $response = Mail::to($request->email)->send(new AccountActivation($user->name, $user->activation_code));

        return response()->json(\Response::success("Success register", $user), 200);
    }

    /**
     * @OA\Get(
     *  path="/auth/detail",
     *  tags={"Auth"},
     *  summary="Detail User",
     *  operationId="detailUser",
     *  security={
     *      {"bearer": {}},
     *  },
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *      )
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     * )
     *)
     **/
    public function detail(Request $request, User $user)
    {
        if ($user = Auth::user()) {
            return response()->json(\Response::success("User found", $user), 200);
        }

        return response()->json(\Response::error_without_data("Unauthorize"), 401);
    }

    /**
     * @OA\Post(
     ** path="/auth/activate",
     *  tags={"Auth"},
     *  summary="Activate",
     *  operationId="activate",
     *
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass param",
     *    @OA\JsonContent(
     *       required={"email","activation_code"},
     *       @OA\Property(property="email", type="string", example=""),
     *       @OA\Property(property="activation_code", type="string", example=""),
     *    ),
     * ),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *      )
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     * )
     *)
     **/
    public function activate(Request $request, User $user)
    {
        $postData = $request->only('email', 'activation_code');

        try {
            $user = User::where([["email", $postData["email"]],["activation_code", $postData['activation_code']]])->first();
            if ($user != null) {
                $user->status = "active";
                $user->save();
                if ($token = $user->createToken('Bule')->accessToken) {
                    return response()->json(\Response::success_without_data("Activation success", [
                        "token" => $token
                    ]), 200);
                }
            }
        } catch (JWTException $e) {
            return response()->json(\Response::error('Could not create token'), 500);
        }

        return response()->json(\Response::error_without_data("Activation failed"), 400);
    }

    /**
     * @OA\Post(
     ** path="/auth/request_reset",
     *  tags={"Auth"},
     *  summary="RequestReset",
     *  operationId="requestReset",
     *
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass param",
     *    @OA\JsonContent(
     *       required={"email"},
     *       @OA\Property(property="email", type="string", example=""),
     *    ),
     * ),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     * )
     *)
     **/
    public function request_reset(Request $request) {
        $postData = $request->only('email');
        $user = User::where([
            ["email", $postData['email']]
        ])->first();

        if ($user != null) {
            $reset_code = mt_rand(1000, 9999);

            $user->reset_code = $reset_code;
            $user->save();

            return response()->json(\Response::success_without_data("Reset code sent"), 200);
        }

        return response()->json(\Response::error_without_data("User not found"), 404);
    }

    /**
     * @OA\Post(
     ** path="/auth/reset_password",
     *  tags={"Auth"},
     *  summary="Reset Password",
     *  operationId="resetPassword",
     *
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass param",
     *    @OA\JsonContent(
     *       required={"email","new_password", "new_password_confirmation", "reset_code"},
     *       @OA\Property(property="email", type="string", example=""),
     *       @OA\Property(property="new_password", type="string", example=""),
     *       @OA\Property(property="new_password_confirmation", type="string", example=""),
     *       @OA\Property(property="reset_code", type="string", example=""),
     *    ),
     * ),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     * )
     *)
     **/
    public function reset_password(Request $request) {
        $postData = $request->only('email', 'new_password', 'new_password_confirmation', 'reset_code');
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'new_password' => 'required|string|min:6|confirmed',
            'reset_code' => 'required|string|digits:4'
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'error' => $validator->errors()
            ), 400);
        }

        $user = User::where([
            ["email", $postData['email']],
            ["reset_code", $postData['reset_code']]
        ])->first();

        if ($user != null) {
            $user->password = Hash::make($postData['new_password']);
            $user->reset_code = '';
            $user->save();

            return response()->json(\Response::success_without_data("Reset password success"), 200);
        }

        return response()->json(\Response::error_without_data("Invalid reset code"), 400);
    }
    
    /**
     * @OA\Post(
     *  path="/auth/update_profile",
     *  tags={"Auth"},
     *  summary="Update Profile",
     *  operationId="updateProfile",
     *  security={
     *      {"bearer": {}},
     *  },
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass param",
     *    @OA\JsonContent(
     *       required={"name","phone", "dob", "gender"},
     *       @OA\Property(property="name", type="string", example=""),
     *       @OA\Property(property="phone", type="string", example="0885829822"),
     *       @OA\Property(property="dob", type="string", example="1995-02-24"),
     *       @OA\Property(property="gender", type="string", example="L"),
     *    ),
     * ),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\JsonContent(
     *          @OA\Property(property="success", type="boolean", example=true),
     *          @OA\Property(property="message", type="string", example="Success"),
     *      ),
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     * )
     *)
     **/
    public function update_profile(Request $request, User $user) {
        if ($user = Auth::user()) {
            $validator = Validator::make($request->only('name', 'phone', 'dob', 'gender'), [
                'name' => 'required|string|max:255',
                'phone' => 'required|numeric|digits_between:3,13',
                'dob' => 'required|date|date_format:Y-m-d',
                'gender' => 'required|string|max:1'
            ]);
    
            if ($validator->fails()) {
                return response()->json(array(
                    'success' => false,
                    'error' => $validator->errors()
                ), 400);
            }

            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->dob = $request->dob;
            $user->gender = $request->gender;
            $user->save();

            $response = array(
                'id' => $user->id,
                'email' => $user->email,
                'name' => $user->name,
                'phone' => $user->phone,
                'dob' => $user->dob,
                'gender' => $user->gender,
            );

            return response()->json(\Response::success("Profile updated", $response), 200);
        }

        return response()->json(\Response::error_without_data("Unauthorize"), 401);
    }


    /**
     * @OA\Post(
     ** path="/auth/save_push_notification_key",
     *  tags={"Auth"},
     *  summary="Save push notification key",
     *  operationId="savePushNotificationKey",
     * security={
     *   {"bearer": {}},
     * },
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass param",
     *    @OA\JsonContent(
     *       required={"key"},
     *       @OA\Property(property="key", type="string", example=""),
     *    ),
     * ),
     * @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *      )
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     * )
     *)
     **/
    public function save_push_notification_key(Request $request) {
        if ($user = Auth::user()) {
            $validator = Validator::make($request->only('key'), [
                'key' => 'required|string',
            ]);
    
            if ($validator->fails()) {
                return response()->json(array(
                    'success' => false,
                    'error' => $validator->errors()
                ), 400);
            }

            $user->push_notif_key = $request->key;
            $user->save();            

            return response()->json(\Response::success_without_data("Push notification key saved"), 200);
        }

        return response()->json(\Response::error_without_data("Unauthorize"), 401);
    }
}
