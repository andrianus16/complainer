<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountActivation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $activation_code)
    {
        $this->name = $name;
        $this->activation_code = $activation_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@mycoplan.id', 'Mycoplan')
                    ->subject('Mycoplan account activation')
                    ->view('emails.confirmation')
                    ->with([
                        'name' => $this->name,
                        'activation_code' => $this->activation_code,
                    ]);
    }
}
