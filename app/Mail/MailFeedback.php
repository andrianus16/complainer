<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailFeedback extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $whatsapp, $feedback)
    {
        $this->name = $name;
        $this->email = $email;
        $this->whatsapp = $whatsapp;
        $this->feedback = $feedback;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@mycoplan.id', 'Mycoplan')
                    ->subject('Feedback from Mycoplan App')
                    ->view('emails.feedback')
                    ->with([
                        'name' => $this->name,
                        'email' => $this->email,
                        'whatsapp' => $this->whatsapp,
                        'feedback' => $this->feedback,
                    ]);
    }
}
