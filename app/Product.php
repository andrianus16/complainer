<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [
        'id'
    ];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    protected $casts = [
        'images' => 'array'
    ];
}
