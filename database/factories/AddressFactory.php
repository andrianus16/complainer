<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'id_user' => factory(App\User::class),
        'label' => $faker->name,
        'alamat' => $faker->text(100),
        'latitude' => $faker->randomFloat(6, -90, 90),
        'longitude' => $faker->randomFloat(6, -180, 180),
        'is_primary' => false,
    ];
});
