<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin;
use Faker\Generator as Faker;

$factory->define(Admin::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => \Hash::make('123'),
        'name' => $faker->name,
        'id_admin_role' => factory(App\AdminRole::class),
        'permissions' => $faker->text(32),
        'push_notif_key' => $faker->text(32),
        'status' => 'active'
    ];
});
