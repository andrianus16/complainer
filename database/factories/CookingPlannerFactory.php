<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CookingPlanner;
use Faker\Generator as Faker;

$factory->define(CookingPlanner::class, function (Faker $faker) {
    return [
        'id_user' => factory(App\User::class),
        'id_recipe' => factory(App\Resep::class),
        'time' => $faker->randomElement(["Pagi", "Siang", "Malam"]),
        'date' => now()
    ];
});
