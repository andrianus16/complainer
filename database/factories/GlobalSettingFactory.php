<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\GlobalSetting;
use Faker\Generator as Faker;

$factory->define(GlobalSetting::class, function (Faker $faker) {
    return [
        'key' => $faker->text(10),
        'value' => $faker->text(200),
    ];
});
