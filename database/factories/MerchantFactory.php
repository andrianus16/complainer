<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Merchant;
use Faker\Generator as Faker;

$factory->define(Merchant::class, function (Faker $faker) {
    return [
        'id_admin' => factory(App\Admin::class),
        'name' => $faker->name,
        'description' => $faker->text(100),
        'logo' => $faker->imageUrl($width = 200, $height = 200),
        'address' => $faker->text(100),
        'latitude' => $faker->randomFloat(6, -90, 90),
        'longitude' => $faker->randomFloat(6, -180, 180),
        'opening_hours' => $faker->time(),
        'closing_hours' => $faker->time(),
        'status' => "active",
    ];
});
