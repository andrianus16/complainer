<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductCategory;
use Faker\Generator as Faker;

$factory->define(ProductCategory::class, function (Faker $faker) {
    return [
        'id_merchant' => factory(App\ProductCategory::class),
        'name' => $faker->text(50),
        'images' => $faker->imageUrl($width = 200, $height = 200),
    ];
});
