<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'id_merchant' => factory(App\Merchant::class),
        'name' => $faker->text(50),
        'price' => $faker->randomNumber(4),
        'discount' => $faker->randomNumber(2),
        'images' => array($faker->imageUrl($width = 200, $height = 200), $faker->imageUrl($width = 200, $height = 200)),
        'description' => Str::random(10),
        'information' => Str::random(10),
        // 'id_ingredient' => factory(App\Ingredient::class),
        'id_product_category' => factory(App\ProductCategory::class),
        'id_product_sub_category' => factory(App\ProductSubCategory::class),
        'quantity' => $faker->randomDigit,
        'id_unit' => factory(App\Unit::class),
        'start' => now(),
        'end' => now(),
    ];
});
