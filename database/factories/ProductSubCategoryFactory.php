<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductSubCategory;
use Faker\Generator as Faker;

$factory->define(ProductSubCategory::class, function (Faker $faker) {
    return [
        'id_product_category' => factory(App\ProductCategory::class),
        'name' => $faker->text(50),
    ];
});
