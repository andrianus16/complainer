<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Produk::class, function (Faker $faker) {
    return [
        'id_store' => factory(App\Store::class),
        'nama' => $faker->text(50),
        'gambar' => $faker->imageUrl($width = 200, $height = 200),
        'satuan' => Str::random(10),
        'quantity' => $faker->randomDigit,
        'harga' => $faker->randomNumber(4),
        'discount' => $faker->randomFloat(2, 0, 100),
        'start_produk' => now(),
        'end_produk' => now(),
    ];
});
