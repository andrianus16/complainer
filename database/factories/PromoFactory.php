<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Promo;
use Faker\Generator as Faker;

$factory->define(Promo::class, function (Faker $faker) {
    return [
        'id_merchant' => factory(App\Merchant::class),
        'description' => Str::random(10),
        'image' => $faker->imageUrl($width = 200, $height = 200),
        'kode_promo' => Str::random(10),
        'kategori' => Str::random(10),
        'syarat_ketentuan' => Str::random(10),
        'nominal' => $faker->randomNumber(4),
        'start_promo' => now(),
        'end_promo' => now()
    ];
});
