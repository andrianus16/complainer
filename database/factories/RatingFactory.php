<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Rating;
use Faker\Generator as Faker;

$factory->define(Rating::class, function (Faker $faker) {
    return [
        'id_user' => factory(App\User::class),
        'id_recipe' => factory(App\Resep::class),
        'value' => mt_rand(1, 5),
    ];
});
