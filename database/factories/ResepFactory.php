<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Resep;
use Faker\Generator as Faker;

$factory->define(Resep::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'images' => json_encode(
            array(
                $faker->imageUrl($width = 200, $height = 100),
                $faker->imageUrl($width = 200, $height = 100)
            )
        ),
        'video' => $faker->imageUrl($width = 200, $height = 100),
        'author' => $faker->randomElement(['mycoplan', 'bule', 'andrianus']),
        'ingredients' => $faker->text(300),
        'steps' => $faker->text(300),
        'production_time' => $faker->randomNumber(1) * 10,
        'portion_min' => 2,
        'portion_max' => 6,
        'difficulty' => $faker->randomElement(['Mudah', 'Sedang', 'Sulit']),
        'id_recipe_type' => factory(App\ResepTipe::class),
        'id_recipe_category' => json_encode($faker->text(300)),
        'supermarkets' => json_encode($faker->text(300)),
    ];
});
