<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ResepKategori;
use Faker\Generator as Faker;

$factory->define(ResepKategori::class, function (Faker $faker) {
    return [
        'name' => $faker->text(10),
        'image' => $faker->imageUrl($width = 200, $height = 100),
    ];
});
