<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ResepTipe;
use Faker\Generator as Faker;

$factory->define(ResepTipe::class, function (Faker $faker) {
    return [
        'name' => $faker->text(50),
    ];
});
