<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Store;
use Faker\Generator as Faker;

$factory->define(Store::class, function (Faker $faker) {
    return [
        'nama' => $faker->text(50),
        'logo' => $faker->imageUrl($width = 200, $height = 200),
        'deskripsi' => $faker->text(50),
    ];
});
