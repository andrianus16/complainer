<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_store');
            $table->foreign('id_store')->references('id')->on('stores');
            $table->string("nama");
            $table->string("gambar");
            $table->string("satuan");
            $table->integer("quantity");
            $table->decimal("harga", 14, 4);
            $table->float("discount", 5, 2);
            $table->date("start_produk");
            $table->date("end_produk");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
}
