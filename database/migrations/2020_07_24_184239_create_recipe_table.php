<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reseps', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->json("images");
            $table->string("video");
            $table->string("author");
            $table->text("ingredients");
            $table->text("steps");
            $table->integer("production_time");
            $table->integer("portion_min");
            $table->integer("portion_max");
            $table->enum("difficulty", ["Mudah", "Sedang", "Sulit"]);
            $table->unsignedBigInteger('id_recipe_type');
            $table->foreign('id_recipe_type')->references('id')->on('resep_tipes');
            $table->json("id_recipe_category");
            $table->json("supermarkets");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe');
    }
}
