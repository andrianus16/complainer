<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_merchant');
            $table->foreign('id_merchant')->references('id')->on('merchants');
            $table->text("description");
            $table->string("kategori");
            $table->string("kode_promo");
            $table->date("start_promo");
            $table->date("end_promo");
            $table->integer("nominal");
            $table->string("image");
            $table->text("syarat_ketentuan");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
