<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_merchant');
            $table->foreign('id_merchant')->references('id')->on('merchants');
            $table->string("name");
            $table->integer("price");
            $table->integer("discount");
            $table->json("images");
            $table->string("description");
            $table->string("information");
            // $table->unsignedBigInteger('id_ingredient');
            // $table->foreign('id_ingredient')->references('id')->on('ingredients');
            $table->unsignedBigInteger('id_product_category');
            $table->foreign('id_product_category')->references('id')->on('product_categories');
            $table->unsignedBigInteger('id_product_sub_category');
            $table->foreign('id_product_sub_category')->references('id')->on('product_sub_categories');
            $table->integer("quantity");
            $table->unsignedBigInteger('id_unit');
            $table->foreign('id_unit')->references('id')->on('units');
            $table->date("start");
            $table->date("end");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
