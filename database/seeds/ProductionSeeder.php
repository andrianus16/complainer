<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\GlobalSetting::class)->create(["key" => "about"]);
        
        factory(App\User::class)->create(["email" => "ivan@gmail.com", "password"=>Hash::make("123456"), "login_method"=>"email"]);
        factory(App\User::class)->create(["email" => "admin@gmail.com", "password"=>Hash::make("123456"), "login_method"=>"email"]);
        factory(App\User::class)->create(["email" => "anuz@gmail.com", "password"=>Hash::make("123456"), "login_method"=>"email"]);
        factory(App\User::class)->create(["email" => "mycoplan@gmail.com", "password"=>Hash::make("123456"), "login_method"=>"email"]);

        factory(App\AdminRole::class)->create(["name" => "super"]);
        factory(App\AdminRole::class)->create(["name" => "merchant"]);

        factory(App\Admin::class)->create(["email" => "admin@gmail.com", "id_admin_role" => 1]);
        factory(App\Admin::class)->create(["email" => "bule@gmail.com", "id_admin_role" => 2]);
        factory(App\Admin::class)->create(["email" => "anuz@gmail.com", "id_admin_role" => 2]);
        factory(App\Admin::class)->create(["email" => "mycoplan@gmail.com", "id_admin_role" => 2]);
        
        factory(App\Unit::class)->create(["name" => "kg"]);
        factory(App\Unit::class)->create(["name" => "gram"]);
        factory(App\Unit::class)->create(["name" => "sdt"]);
        factory(App\Unit::class)->create(["name" => "sdm"]);
        factory(App\Unit::class)->create(["name" => "ml"]);
        factory(App\Unit::class)->create(["name" => "liter"]);
        factory(App\Unit::class)->create(["name" => "bungkus"]);
        factory(App\Unit::class)->create(["name" => "botol"]);

        factory(App\Merchant::class)->create(["id_admin" => 1, "name" => "Carrefour"]);
        factory(App\Merchant::class)->create(["id_admin" => 2, "name" => "Indomart"]);
        factory(App\Merchant::class)->create(["id_admin" => 2, "name" => "Alfamart"]);
        factory(App\Merchant::class)->create(["id_admin" => 3, "name" => "Giant"]);
        factory(App\Merchant::class)->create(["id_admin" => 4, "name" => "Hero"]);

        factory(App\Promo::class)->create(["id_merchant" => 1]);
        factory(App\Promo::class)->create(["id_merchant" => 2]);
        factory(App\Promo::class)->create(["id_merchant" => 3]);
        factory(App\Promo::class)->create(["id_merchant" => 4]);
        factory(App\Promo::class)->create(["id_merchant" => 5]);

        factory(App\ProductCategory::class)->create(["id_merchant" => 1, "name" => "Daging"]);
        factory(App\ProductCategory::class)->create(["id_merchant" => 1, "name" => "Permen"]);
        factory(App\ProductCategory::class)->create(["id_merchant" => 1, "name" => "Sayur"]);
        factory(App\ProductCategory::class)->create(["id_merchant" => 2, "name" => "Buah"]);
        factory(App\ProductCategory::class)->create(["id_merchant" => 3, "name" => "Nasi"]);
        factory(App\ProductCategory::class)->create(["id_merchant" => 4, "name" => "Mie"]);

        factory(App\ProductSubCategory::class)->create(["id_product_category" => 1, "name" => "Daging Sapi"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 1, "name" => "Daging Ikan"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 1, "name" => "Daging Babi"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 1, "name" => "Daging Ayam"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 2, "name" => "Permen Asem"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 2, "name" => "Permen Manis"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 3, "name" => "Sayur Pahit"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 4, "name" => "Buah berduri"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 5, "name" => "Nasi merah"]);
        factory(App\ProductSubCategory::class)->create(["id_product_category" => 6, "name" => "Mie kriting"]);

        factory(App\Product::class)->create(["id_merchant" => 1, "name" => "Steak sapi", "id_product_category" => 1, "id_product_sub_category" => 1, "id_unit" => 1]);
        factory(App\Product::class)->create(["id_merchant" => 1, "name" => "Steak ayam", "id_product_category" => 1, "id_product_sub_category" => 4, "id_unit" => 1]);
        factory(App\Product::class)->create(["id_merchant" => 1, "name" => "Steak ikan", "id_product_category" => 1, "id_product_sub_category" => 2, "id_unit" => 1]);
        factory(App\Product::class)->create(["id_merchant" => 1, "name" => "Sugus", "id_product_category" => 2, "id_product_sub_category" => 6, "id_unit" => 2]);
        factory(App\Product::class)->create(["id_merchant" => 1, "name" => "Sayur Bayam", "id_product_category" => 3, "id_product_sub_category" => 7, "id_unit" => 2]);
        factory(App\Product::class)->create(["id_merchant" => 2, "name" => "Sup buah","id_product_category" => 4, "id_product_sub_category" => 8, "id_unit" => 2]);
        factory(App\Product::class)->create(["id_merchant" => 2, "name" => "Jus Jeruk", "id_product_category" => 4, "id_product_sub_category" => 8, "id_unit" => 3]);
        factory(App\Product::class)->create(["id_merchant" => 3, "name" => "Nasi goreng", "id_product_category" => 5, "id_product_sub_category" => 9, "id_unit" => 1]);
        factory(App\Product::class)->create(["id_merchant" => 3, "name" => "Nasi aking", "id_product_category" => 5, "id_product_sub_category" => 9, "id_unit" => 1]);
        factory(App\Product::class)->create(["id_merchant" => 4, "name" => "Mie rebus", "id_product_category" => 6, "id_product_sub_category" => 10, "id_unit" => 2]);
        
        factory(App\ResepKategori::class)->create(["name" => "Indonesian"]);
        factory(App\ResepKategori::class)->create(["name" => "Korean"]);
        factory(App\ResepKategori::class)->create(["name" => "Japanese"]);

        factory(App\ResepTipe::class)->create(["name" => "Hidangan Pembuka"]);
        factory(App\ResepTipe::class)->create(["name" => "Hidangan Utama"]);
        factory(App\ResepTipe::class)->create(["name" => "Hidangan Penutup"]);
        factory(App\ResepTipe::class)->create(["name" => "Minuman"]);
        factory(App\ResepTipe::class)->create(["name" => "Camilan"]);

        $ingredients = array(
            array(
                "name" => "Beras",
                "qty" => "200",
                "unit" => "gram",
            ),
            array(
                "name" => "Garam",
                "qty" => "3",
                "unit" => "sdt",
            ),
            array(
                "name" => "Telur",
                "qty" => "2",
                "unit" => "butir",
            ),
        );

        $steps = array(
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed, voluptatibus.",
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed, voluptatibus.",
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed, voluptatibus.",
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed, voluptatibus.",
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed, voluptatibus."
        );

        factory(App\Resep::class)->create(["id_recipe_type" => 1, "title" => "Resep Ayam KFC mantap", "id_recipe_category" => json_encode(array(1,2)), "ingredients" => json_encode($ingredients), "steps" => json_encode($steps)]);
        factory(App\Resep::class)->create(["id_recipe_type" => 1, "title" => "Resep Ayam Tiren mantap", "id_recipe_category" => json_encode(array(1,3)), "ingredients" => json_encode($ingredients), "steps" => json_encode($steps)]);
        factory(App\Resep::class)->create(["id_recipe_type" => 2, "title" => "Resep Indomie Rebus mantap", "id_recipe_category" => json_encode(array(2,3)), "ingredients" => json_encode($ingredients), "steps" => json_encode($steps)]);
        factory(App\Resep::class)->create(["id_recipe_type" => 3, "title" => "Resep Pangsit Rebus mantap", "id_recipe_category" => json_encode(array(1,2,3)), "ingredients" => json_encode($ingredients), "steps" => json_encode($steps)]);
        factory(App\Resep::class)->create(["id_recipe_type" => 2, "title" => "Resep Nasi Rebus mantap", "id_recipe_category" => json_encode(array(1)), "ingredients" => json_encode($ingredients), "steps" => json_encode($steps)]);
        

        factory(App\Address::class)->create(["id_user" => 1, "label" => "Alamat Utama", "alamat" => "Jl Garuda", "latitude" => -22.76632, "longitude" => -98.59466, "is_primary" => true]);
        factory(App\Address::class)->create(["id_user" => 1, "label" => "Alamat 1", "alamat" => "Jl Gunung sahari", "is_primary" => false]);
        factory(App\Address::class)->create(["id_user" => 1, "label" => "Alamat 2", "alamat" => "Jl Pelni", "is_primary" => false]);
        factory(App\Address::class)->create(["id_user" => 1, "label" => "Alamat 3", "alamat" => "Jl Bungur", "is_primary" => false]);

        factory(App\CookingPlanner::class)->create(["id_user" => 1, "id_recipe" => 1, "time" => "Pagi"]);
        factory(App\CookingPlanner::class)->create(["id_user" => 1, "id_recipe" => 2, "date" => new Carbon('2016-01-23')]);
        factory(App\CookingPlanner::class)->create(["id_user" => 1, "id_recipe" => 3]);



        factory(App\Rating::class)->create(["id_user" => 1, "id_recipe" => 1]);
        factory(App\Rating::class)->create(["id_user" => 1, "id_recipe" => 2]);
        factory(App\Rating::class)->create(["id_user" => 1, "id_recipe" => 3]);
        factory(App\Rating::class)->create(["id_user" => 1, "id_recipe" => 4]);
        factory(App\Rating::class)->create(["id_user" => 1, "id_recipe" => 5]);
        factory(App\Rating::class)->create(["id_user" => 2, "id_recipe" => 1]);
        factory(App\Rating::class)->create(["id_user" => 2, "id_recipe" => 2]);
        factory(App\Rating::class)->create(["id_user" => 2, "id_recipe" => 3]);
        factory(App\Rating::class)->create(["id_user" => 2, "id_recipe" => 4]);
        factory(App\Rating::class)->create(["id_user" => 2, "id_recipe" => 5]);
        factory(App\Rating::class)->create(["id_user" => 3, "id_recipe" => 1]);
        factory(App\Rating::class)->create(["id_user" => 3, "id_recipe" => 2]);
        factory(App\Rating::class)->create(["id_user" => 3, "id_recipe" => 3]);
        factory(App\Rating::class)->create(["id_user" => 3, "id_recipe" => 4]);
        factory(App\Rating::class)->create(["id_user" => 3, "id_recipe" => 5]);
        factory(App\Rating::class)->create(["id_user" => 4, "id_recipe" => 1]);
        factory(App\Rating::class)->create(["id_user" => 4, "id_recipe" => 2]);
        factory(App\Rating::class)->create(["id_user" => 4, "id_recipe" => 3]);
        factory(App\Rating::class)->create(["id_user" => 4, "id_recipe" => 4]);
        factory(App\Rating::class)->create(["id_user" => 4, "id_recipe" => 5]);
    }
}
