<div>
    Hi {{ $name }}, thank you for registering on Mycoplan <br><br>

    Here is your activation code: <strong>{{ $activation_code }}</strong>
</div>