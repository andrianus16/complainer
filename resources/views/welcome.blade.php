<!DOCTYPE html>
<html>
	<head>
		<title>Mycoplan!</title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<style>
			a,
			a:link,
			a:visited,
			a:active,
			a:hover {
				text-decoration: none;
			}
		</style>
	</head>
	<body style="position: relative; min-height: 100%; margin: 0px; padding: 0px; background-color: #ffa500; font-family: Arial, sans-serif;">
		<div style="position: absolute; top: 0px; left: 0px; width: 100%; min-height: 100%; background: linear-gradient(0deg, #ffa500 1%, #FFC107 100%);">

			<div style="margin-top: 150px; text-align: center;">
				<a href="https://www.mycoplan.id/">
					<img src="{{ asset('logo.svg') }}" alt="Mycoplan" style="width: 100%; min-width: 100px; max-width: 502px; height: auto; padding: 16px; box-sizing: border-box;"/><br/>
				</a>
			</div>

		</div>
	</body>
</html>
