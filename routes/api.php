<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::Group([
    'namespace' => 'api',
    'prefix' => 'auth'
], function () {
    Route::get('facebook/callback', 'UserController@facebook_callback'); // TODO route buat test
    Route::get('google/callback', 'UserController@google_callback'); // TODO route buat test
    Route::post('facebook/login', 'UserController@facebook_login');
    Route::post('google/login', 'UserController@google_login');
    Route::post('login', 'UserController@login')->name('login');
    Route::post('register', 'UserController@register')->name('register');
    Route::get('detail', 'UserController@detail')->name('detail');
    Route::post('activate', 'UserController@activate')->name('activate');
    Route::post('request_reset', 'UserController@request_reset')->name('request_reset');
    Route::post('reset_password', 'UserController@reset_password')->name('reset_password');
    Route::post('update_profile', 'UserController@update_profile')->name('update_profile');
    Route::post('save_push_notification_key', 'UserController@save_push_notification_key')->name('save_push_notification_key');
});

Route::Group([
    'namespace' => 'api',
    'middleware' => 'auth:api'
], function () {
    // resep kategori
    Route::get('recipe-categories', 'ResepKategoriController@index');
    Route::get('recipe-category/{category_id}/recipes', 'ResepKategoriController@recipeByCategory');
    Route::get('recipe-categories/{category_id}/similar', 'ResepKategoriController@similarRecipeCategory');

    // resep
    Route::get('recipes', 'RecipeController@recipes');
    Route::get('recipe/{id_recipe}', 'RecipeController@recipeDetail');
    Route::get('recipes/search', 'RecipeController@search');
    
    // resep favorit
    Route::get('favorite-recipes', 'ResepFavoritController@index');
    Route::post('recipe/toggle-favorite', 'ResepFavoritController@toggle');
    
    // rating
    Route::post('recipe/rate', 'RatingController@store');

    // product
    Route::get('product/search', 'ProductController@search');
    Route::get('product/{id_product}', 'ProductController@detail');
    Route::get('product/{id_product}/similar', 'ProductController@similar');

    // product category
    Route::get('product-category/{id_category}/subcategories', 'ProductCategoryController@subcategories');
    
    // merchant
    Route::get('merchant', 'MerchantController@index');
    Route::get('merchant/{id_merchant}', 'MerchantController@detail');
    Route::get('merchant/{id_merchant}/categories', 'MerchantController@categories');

    // promo
    Route::get('promo', 'PromoController@index');
    Route::get('promo/{id_promo}', 'PromoController@detail');

    // address
    Route::get('addresses', 'AddressController@addresses');
    Route::post('address', 'AddressController@store');
    Route::put('address/{address_id}', 'AddressController@update');
    Route::put('address/{address_id}/makePrimary', 'AddressController@makePrimary');
    Route::delete('address/{address_id}', 'AddressController@destroy');
    
    // cooking planner
    Route::get('cooking-planner', 'CookingPlannerController@index');
    Route::get('cooking-planner/list-date', 'CookingPlannerController@listDate');
    Route::post('cooking-planner', 'CookingPlannerController@store');
    Route::delete('cooking-planner/{cookingplanner_id}', 'CookingPlannerController@destroy');

    // feedback
    Route::post('feedback', 'FeedbackController@store');

    // global
    Route::get('global', 'GlobalSettingController@index');
});
